#Бинарный поиск

def binary_search(arr, search_value):
    first = 0
    last = len(arr) - 1

    while first <= last:
        # Находим середину списка
        mid = (last + first) // 2
        if search_value == arr[mid]:
            print(f'Результат число {search_value} на позиции {mid + 1}')
            break;
        elif search_value > arr[mid]:
            # Т.к. список отсортирован мы может сразу выбрать правую или левую его половину
            # для дальнейшего поиска
            first = mid + 1
        elif search_value < arr[mid]:
            last = mid - 1
    else:
        print('Not founded value')


a = [1, 2, 3, 5, 6, 7, 8]

binary_search(a, 8)
