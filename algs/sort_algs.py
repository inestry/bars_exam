# Сортировка выбором
def find_min_element(arr):
    min_elem = arr[0]
    for i in range(1, len(arr)):
        if min_elem > arr[i]:
            min_elem = arr[i]
    return min_elem


def choice_sort(arr):
    new_arr = []
    for i in range(len(arr)):
        min_elem = find_min_element(arr)
        new_arr.append(min_elem)
        arr.remove(min_elem)
    return new_arr


a = [5, 85, 47, 6, 23, 2, 3, 69, 0]
print(str(choice_sort(a)))
