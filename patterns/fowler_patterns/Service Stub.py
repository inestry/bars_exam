class MessageSender:
    #  Интерфейс к внешней службе для отправки сообщений
    SUCCESS = 0

    def send(self, msg, **kwargs):
        pass


class TestMessageService:
    #  Служба для тестирования без обращения к внешней службе отправки
    SUCCESS = 0

    def send(self, msg, **kwargs):
        print(msg)
        return self.SUCCESS


class Order:
    __slots__ = ['order_id', 'amount', 'symbol']


class MessageGateway:
    CONFIRM = 'CNFRM'
    msg_sender = MessageSender

    def send_confirmation(self, order):
        kwargs = {'order_id': order.order_id,
                  'amount': order.amount,
                  'symbol': order.symbol}

        self.send(self.CONFIRM, **kwargs)

    def send(self, msg, **kwargs):
        return_code = self.do_send(msg, **kwargs)
        if return_code != self.msg_sender.SUCCESS:
            raise Exception(f"Ошибка при отправке сообщения: {return_code}")

    def do_send(self, msg, **kwargs):
        return self.msg_sender().send(msg, **kwargs)


class MessageGatewayForTest(MessageGateway):
    msg_sender = TestMessageService

