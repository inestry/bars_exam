class DataMap:
    def __init__(self, domain_class, table_name, column_maps=None):
        self.domain = domain_class
        self.table_name = table_name
        if column_maps:
            self.column_maps = column_maps
        else:
            self.column_maps = []

    def load_attrs(self, domain_object, column_values):
        for num, column_map in enumerate(self.column_maps):
            if hasattr(domain_object, column_map.attr_name):
                setattr(domain_object, column_map.attr_name,
                        column_values[num])


class ColumnMap:
    def __init__(self, column_name, attr_name, field, data_map):
        self.column_name = column_name
        self.attr_name = attr_name
        self.field = field
        self.data_map = data_map


class Mapper:

    def find_object(self, key):
        sql_statement = (f"SELECT {self.data_map.columns_list} " 
                         f"FROM {self.data_map.table_name} "
                         f"WHERE id = {key}")

        conn = DBConnection().get_connection()
        cur = conn.cursor()
        cur.execute(sql_statement)
        row = cur.fetchone()
        result = self.load(row)
        return result

    def load(self, key, row):
        result = self.data_map.domain()
        result.id = key
        self.data_map.load_attrs(result, row)

        return result

