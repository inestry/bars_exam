class Address:
    def __init__(self, country, street, house_num):
        self.country = country
        self.street = street
        self.house_num = house_num


class AddressRemoteFacade(Address):
    def get_address(self):
        address = f"{self.country} {self.street} {self.house_num}"
        return address

    def set_address(self, address):
        # Предположим что страны и улицы состоят из одного слова
        self.country, self.street, self.house_num = address.split('')
