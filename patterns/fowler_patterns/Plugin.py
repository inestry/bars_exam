
class BaseService:
    @staticmethod
    def process():
        print("Hello it's base service")


class MyApplication:
    def __init__(self, *, plugins=None):
        self.internal_modules = [BaseService()]
        if not plugins:
            self._plugins = []
        else:
            self._plugins = plugins

    def get_modules(self):
        return [*self.internal_modules, *self._plugins]

    def run(self):
        for module in self.get_modules():
            module.process()


# External plugins:
class FirstCustomService:
    @staticmethod
    def process():
        print("Hello it's first custom service")


class SecondCustomService:
    @staticmethod
    def process():
        print("Hello it's second custom service")


app = MyApplication()
app.run()
print('********')
app = MyApplication(plugins=[FirstCustomService()])
app.run()
print('********')
app = MyApplication(plugins=[FirstCustomService(), SecondCustomService()])
app.run()
