"""
Задача: Разделить Active Record на два компонента
    1)Объектная модель предметной области
    2)Объект Data Mapper, который отвечает за чтение из базы данных и создание
      экземпляров модели предметной области

У нас есть таблица Worker
( id integer / name varchar / tab_num varchar / grade integer)

"""
import psycopg2


# Класс, который отвечает за подключение к СУБД
class DBConnection:
    def __init__(self):
        self.connect_params = ("dbname=postgres user=bars_web_bb " 
                               "password=bars_web_bb host=127.0.0.1 port=5432")

    def connect(self):
        conn = psycopg2.connect(self.connect_params)
        return conn


class Worker:
    __slots__ = ['id', 'name', 'tab_num', 'grade']

    def __init__(self, worker_id=None):
        if worker_id:
            self.id = worker_id

    def is_receive_bonus(self):
        result = False
        if self.grade > 5:
            result = True

        return result


class WorkerDataMapper:
    @classmethod
    def get_connection(cls):
        if hasattr(cls, 'connection'):
            return cls.connection
        else:
            cls.connection = DBConnection().connect()

    def insert(id, name, tab_num, grade):
        conn = WorkerDataMapper.get_connection()
        sql_string = (f"INSERT INTO public.worker VALUES ({id},'{name}',"
                      f"'{tab_num}',{grade})")
        cur = conn.cursor()
        cur.execute(sql_string)
        conn.commit()
        worker = Worker(id)
        worker.name = name
        worker.tab_num = tab_num
        worker.grade = grade

        return worker

    def find_by_tab_num(tab_num):
        conn = WorkerDataMapper.get_connection()
        select_string = (f"SELECT id, name, tab_num, grade FROM public.worker "
                         f"WHERE tab_num = '{tab_num}'")

        cur = conn.cursor()
        cur.execute(select_string)
        id, name, tab_num, grade = cur.fetchone()
        worker = Worker(id)
        worker.name = name
        worker.tab_num = tab_num
        worker.grade = grade

        return worker


my_worker = WorkerDataMapper.find_by_tab_num('A003002')
new_worker = WorkerDataMapper.insert(id=7, name='Семенова',
                                     tab_num='A003007', grade=5)


