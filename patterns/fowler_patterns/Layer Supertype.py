"""Supertype Domain Layer"""


class DomainModel:
    def __init__(self, idx, first_name, last_name):
        self.idx = idx
        self.first_name = first_name
        self.last_name = last_name


class Worker(DomainModel):
    def __init__(self, idx, first_name, last_name, tab_num):
        super().__init__(idx, first_name, last_name)
        self.tab_num = tab_num


class Student(DomainModel):
    def __init__(self, idx, first_name, last_name, group_num):
        super().__init__(idx, first_name, last_name)
        self.group_num = group_num
