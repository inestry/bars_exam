import json


def get_persons():
    persons_json = """{"persons":[{
                                   "first_name": "Иван",
                                   "last_name": "Иванов",
                                   "age": 25
                                   },
                                  {
                                   "first_name": "Петр",
                                   "last_name": "Иванов",
                                   "age": 30
                                  },
                                  {
                                   "first_name": "Мария",
                                   "last_name": "Семенова",
                                   "age": 25
                                  }]
                        }"""
    return persons_json


# Model
class Person:
    def __init__(self, first_name, last_name, age):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age

    @property
    def name(self):
        return f'{self.last_name} {self.first_name}'

    @staticmethod
    def get_all():
        result = []
        json_list = json.loads(get_persons())
        for item in json_list['persons']:
            person = Person(item['first_name'], item['last_name'], item['age'])
            result.append(person)
        return result


class View:
    @staticmethod
    def show_all_view(items_list):
        print(f'В Базе Данных хранится {len(items_list)} пользователей. '
              f'Вот их список:')
        for item in items_list:
            print(f'{item.name} Возраст: {item.age}')

    @staticmethod
    def start_view():
        print("Это простейший пример реализации MVC"
              "Хотите посмотреть всех пользователей в Базе Данных? [y/n]")

    @staticmethod
    def end_view():
        print("До свидания!")


class Controller:

    @staticmethod
    def show_all():
        people_in_db = Person.get_all()
        return View.show_all_view(people_in_db)

    def start(self):
        View.start_view()
        inputed_value = input()
        if inputed_value == 'y':
            return self.show_all()
        else:
            return View.end_view()


Controller().start()