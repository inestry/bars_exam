class RegistryHolder(type):

    REGISTRY = {}

    def __new__(mcs, name, bases, attrs):
        new_cls = type.__new__(mcs, name, bases, attrs)
        """
         Добавляем классы в реестр, ключем будет имя класса
        """
        mcs.REGISTRY[new_cls.__name__] = new_cls
        return new_cls

    @classmethod
    def get_registry(mcs):
        return dict(mcs.REGISTRY)


class BaseRegisteredClass(metaclass=RegistryHolder):
    """
    Все плотомки будут включены в реестр
    """


class ClassRegistree1(BaseRegisteredClass):
    def __init__(self, *args, **kwargs):
        pass


def register_classes():
    print(RegistryHolder.get_registry())
    # {'BaseRegisteredClass': <class '__main__.BaseRegisteredClass'>,
    # 'ClassRegistree1': <class '__main__.ClassRegistree1'>}

    class ClassRegistree2(BaseRegisteredClass):
        def __init__(self, *args, **kwargs):
            pass

    print(RegistryHolder.get_registry())
    # {'BaseRegisteredClass': <class '__main__.BaseRegisteredClass'>,
    # 'ClassRegistree1': <class '__main__.ClassRegistree1'>,
    # 'ClassRegistree2': <class '__main__.register_classes.<locals>.ClassRegistree2'>}


register_classes()