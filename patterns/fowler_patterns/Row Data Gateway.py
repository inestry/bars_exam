"""
У нас есть таблица Worker
( id integer / name varchar / tab_num varchar / grade integer)

*Разница между Row Data Gateway и Table Data Gateway в том, что
 Table Data Gateway будет возвращать Record Set

"""
import psycopg2


# Класс, который отвечает за подключение к СУБД
class DBConnection:
    def __init__(self):
        self.connect_params = ("dbname=postgres user=bars_web_bb " 
                               "password=bars_web_bb host=127.0.0.1 port=5432")

    def connect(self):
        conn = psycopg2.connect(self.connect_params)
        return conn


class WorkerGateway:
    __slots__ = ['id', 'name', 'tab_num', 'grade']

    def __init__(self, worker_id=None):
        if worker_id:
            self.id = worker_id

    @classmethod
    def get_connection(cls):
        if hasattr(cls, 'connection'):
            return cls.connection
        else:
            cls.connection = DBConnection().connect()

    def insert(id, name, tab_num, grade):
        conn = WorkerGateway.get_connection()
        sql_string = (f"INSERT INTO public.worker VALUES ({id},'{name}',"
                      f"'{tab_num}',{grade})")
        cur = conn.cursor()
        cur.execute(sql_string)
        conn.commit()
        worker = WorkerGateway(id)
        worker.name = name
        worker.tab_num = tab_num
        worker.grade = grade

        return worker

    def update(self, **kwargs):
        conn = self.get_connection()

        update_values = ' '.join([
            f'{key}={value} ' for key, value in kwargs.items()])
        sql_string = (f'UPDATE public.worker SET {update_values} '
                      f'WHERE id={self.id}')
        cur = conn.cursor()
        cur.execute(sql_string)
        conn.commit()
        for key, value in kwargs.items():
            self.__setattr__(key, value)

    def delete(self):
        conn = self.get_connection()
        sql_string = (f'DELETE FROM public.worker '
                      f'WHERE id={self.id}')
        cur = conn.cursor()
        cur.execute(sql_string)
        conn.commit()
        del self

    def find_by_tab_num(tab_num):
        conn = WorkerGateway.get_connection()
        select_string = (f"SELECT id, name, tab_num, grade FROM public.worker "
                         f"WHERE tab_num = '{tab_num}'")

        cur = conn.cursor()
        cur.execute(select_string)
        id, name, tab_num, grade = cur.fetchone()
        worker = WorkerGateway(id)
        worker.name = name
        worker.tab_num = tab_num
        worker.grade = grade

        return worker


my_worker = WorkerGateway.find_by_tab_num('A003002')
my_worker.update(grade=5)
new_worker = WorkerGateway.insert(id=7, name='Семенова',
                                  tab_num='A003007', grade=5)
new_worker.delete()

print(new_worker)