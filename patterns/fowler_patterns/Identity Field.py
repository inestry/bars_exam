""" Пример использования числового ключа - id
 У нас есть таблица Worker
( id integer / name varchar / tab_num varchar / grade integer)
"""


class Worker:
    __slots__ = ['id', 'name', 'tab_num', 'grade']


class Mapper:
    def abstract_find(self, id):
        row = self.find_row(id)
        if not row:
            raise Exception('There are no key value in DB')
        return self.find(row)

    def find(self, row):
        result = self.create_domain_object()  # Создаем экземпляр domain object
        self.load(result, row)  # Заполняем атрибуты domain object из базы
        return result

    def find_row(self, id):
        filter_value = f'id = {id}'  # Sql для поиска
        results = self.table.select(filter_value)
        if results:
            return results[0]


class WorkerMapper(Mapper):
    create_domain_object = Worker

    @staticmethod
    def load(result, row):
        result.id = row['id']
        result.name = row['name']
        result.tab_num = row['tab_num']
        result.grade = row['grade']

    @staticmethod
    def save(value, row):
        row['id'] = value.id
        row['name'] = value.name
        row['tab_num'] = value.tab_num
        row['grade'] = value.grade

    def insert(self, worker):
        row = self.table.newRow()
        id = self.get_next_id()
        row.id = id
        self.save(worker, row)
        self.table.rows.add(row)
        return id


founded_worker = WorkerMapper.abstract_find(1)

new_worker = Worker()
new_worker.name = 'Vasya'
new_worker.tab_num = 'A1258BFC'
new_worker.grade = 5
new_worker_id = WorkerMapper.insert(new_worker)


