class Worker:
    __slots__ = ['id', 'name', 'tab_num', 'grade']

    def __init__(self, worker_id, name, tab_num, grade):
        if worker_id:
            self.id = worker_id
            self.name = name
            self.tab_num = tab_num
            self.grade = grade

    def change_tab_num(self, new_tab_num):
        self.tab_num = new_tab_num

    def is_receive_bonus(self):
        result = False
        if self.grade > 5:
            result = True

        return result
