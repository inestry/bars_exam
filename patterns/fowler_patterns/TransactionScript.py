"""
    У нас есть Worker, которого переводят с одной должности на
    другую, обновляя его табельный номер.
    Таблицы
    У нас есть таблица Worker
    Таблица должностей Position
    И таблица назначений WorkerOnPosition
    (worker_id integer / position_id integer)

"""
worker_id = 1050
new_position_id = 15


class DBConnection:
    def __init__(self):
        self.connect_params = ("dbname=postgres user=bars_web_bb " 
                               "password=bars_web_bb host=127.0.0.1 port=5432")

    def connect(self):
        conn = psycopg2.connect(self.connect_params)
        return conn


def move_worker(worker_id, new_position_id):
    conn = DBConnection().connect()
    cur = conn.cursor()

    delete_sql_str = (f"DELETE FROM WorkerOnPosition "
                      f"WHERE worker_id = {worker_id};")

    insert_sql_str = (f"INSERT INTO WorkerOnPosition "
                      f"VALUES ({worker_id}, {new_position_id};)")

    try:
        cur.execute(delete_sql_str)
        cur.execute(insert_sql_str)
        conn.commit()
    except Exception as e:
        conn.rollback()


move_worker(worker_id, new_position_id)
