"""
Active Record выглядит очень похоже на Row Data Gateway ,
 но с одним отличием : Активная запись содержит доменную логику

"""


class Worker(WorkerGateway):

    def is_receive_bonus(self):
        result = False
        if self.grade > 5:
            result = True
            
        return result


my_worker = Worker.find_by_tab_num('A003002')
need_pay_bonus = my_worker.is_receive_bonus()
