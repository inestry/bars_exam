from decimal import Decimal


class Currency:
    code = "XXX"
    exchange_rate = Decimal("1.0")

    def __init__(self, code=""):
        self.code = code

    def __repr__(self):
        return self.code

    def set_exchange_rate(self, rate):
        if not isinstance(rate, Decimal):
            rate = Decimal(str(rate))
        self.exchange_rate = rate


DEFAULT_CURRENCY = Currency('RUB')


def set_default_currency(code):
    global DEFAULT_CURRENCY
    DEFAULT_CURRENCY = Currency[code]


class Money:
    amount = Decimal("0.0")
    currency = DEFAULT_CURRENCY

    def __init__(self, amount=Decimal("0.0"), currency=None):
        if not isinstance(amount, Decimal):
            amount = Decimal(str(amount))
        self.amount = amount
        if not currency:
            self.currency = DEFAULT_CURRENCY
        else:
            self.currency = currency

    def __repr__(self):
        return f'{self.currency} {self.amount}'

    def __pos__(self):
        return Money(amount=self.amount, currency=self.currency)

    def __neg__(self):
        return Money(amount=-self.amount, currency=self.currency)

    def __add__(self, other):
        if isinstance(other, Money):
            if self.currency == other.currency:
                return Money(amount=self.amount + other.amount,
                             currency=self.currency)
            else:
                s = self.convert_to_default()
                other = other.convert_to_default()
                return Money(amount=s.amount + other.amount,
                             currency=DEFAULT_CURRENCY)
        else:
            return Money(amount=self.amount + Decimal(str(other)),
                         currency=self.currency)

    def __sub__(self, other):
        if isinstance(other, Money):
            if self.currency == other.currency:
                return Money(amount=self.amount - other.amount,
                             currency=self.currency)
            else:
                s = self.convert_to_default()
                other = other.convert_to_default()
                return Money(amount=s.amount - other.amount,
                             currency=DEFAULT_CURRENCY)
        else:
            return Money(amount=self.amount - Decimal(str(other)),
                         currency=self.currency)

    def convert_to_default(self):
        return Money(amount=self.amount * self.currency.exchange_rate,
                     currency=DEFAULT_CURRENCY)

    __radd__ = __add__
    __rsub__ = __sub__

    def __eq__(self, other):
        if isinstance(other, Money):
            return (self.amount == other.amount) and (
                        self.currency == other.currency)
        return self.amount == Decimal(str(other))

    def __ne__(self, other):
        result = self.__eq__(other)
        if result is NotImplemented:
            return result
        return not result

    def __lt__(self, other):
        if isinstance(other, Money):
            if self.currency == other.currency:
                return self.amount < other.amount
            else:
                raise TypeError("Нельзя сравнить разные валюты")
        else:
            return self.amount < Decimal(str(other))

    def __gt__(self, other):
        if isinstance(other, Money):
            if self.currency == other.currency:
                return self.amount > other.amount
            else:
                raise TypeError("Нельзя сравнить разные валюты")
        else:
            return self.amount > Decimal(str(other))

    def __le__(self, other):
        return self < other or self == other

    def __ge__(self, other):
        return self > other or self == other


usd = Currency("USD")
usd.set_exchange_rate(76.5)

five_usd = Money(5, usd)
five_rub = Money(5)

print(five_rub + five_usd)
