from abc import ABC

# Lazy initializaton
class Education(EducationActiveRecord):
    pass


class Worker(WorkerActiveRecord):
    @property
    def education(self):
        if self.education is None:
            self.education = Education.find(self.id)
        return self.education


# Value Holder
class ValueHolder:
    def __init__(self, loader):
        #value имеет тип Product
        self.value = None
        # loader имеет тип ProductLoader
        self.loader = loader

    def get_value(self):
        if not self.value:
            self.value = self.loader.load()
        return self.value


class ProductLoader:
    def load(self):
        # возвращает найденный в бд Product
        # делая sql запрос
        return ProductDataMapper.findForSupplier(self.id)


class Order:
    def __init__(self):
        self.products = ValueHolder(ProductLoader)

    @property
    def products(self):
        # products имеет тип ValueHolder
        return self.products.get_value()




