"""
    Однозначная ссылка
"""


class Artist:
    __slots__ = ['id', 'name']


class AlbumMapper:
    @staticmethod
    def load(result, row):
        result.id = row['id']
        result.title = row['title']
        result.artist = MapperRegistry.getMapper(Artist).find(row['artist'])

    @staticmethod
    def save(value, row):
        row['id'] = value.id
        row['title'] = value.title
        row['artist'] = value.artist.id


    def insert(self, album):
        row = self.table.newRow()
        id = self.get_next_id()
        row.id = id
        self.save(album, row)
        self.table.rows.add(row)
        return id

class ArtistMapper(Mapper):
    pass
 # реализация аналогична Identity Field


class Album:
    __slots__ = ['id', 'title', 'artist']

    def __init__(self, album_id, title, artist):
        self.id = album_id
        self.title = title
        self.artist = ValueHolder(ArtistLoader)

    @property
    def artist(self):
        return self.artist.get_value()


class ValueHolder:
    def __init__(self, loader):
        self.value = None
        self.loader = loader

    def get_value(self):
        if not self.value:
            self.value = self.loader.load()
        return self.value


class ArtistLoader:
    def load(self):
        # возвращает найденный в бд Artist
        # делая sql запрос
        return ArtistMapper.find(self.id)


founded_artist = ArtistMapper.abstract_find(1)

new_album = Album()
new_album.title = 'Vasya'
new_album.artist = founded_artist
new_worker_id = AlbumMapper.insert(new_album)



