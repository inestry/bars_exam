
class DomainObject:
    def mark_new(self):
        UnitOfWork.get_current().register_new(self)

    def mark_dirty(self):
        UnitOfWork.get_current().register_dirty(self)

    def mark_removed(self):
        UnitOfWork.get_current().register_removed(self)

    def mark_clean(self):
        UnitOfWork.get_current().register_clean(self)


class UnitOfWork:

    def __init__(self):
        self.new_objects = []
        self.dirty_objects = []
        self.removed_objects = []

    def register_new(self, domain_object):
        if (domain_object not in self.new_objects
           and domain_object not in self.dirty_objects
           and domain_object not in self.removed_objects):

            self.new_objects.append(domain_object)

    def register_dirty(self, domain_object):
        if (domain_object not in self.new_objects
                and domain_object not in self.dirty_objects
                and domain_object not in self.removed_objects):
            self.dirty_objects.append(domain_object)

    def register_removed(self, domain_object):
        if domain_object in self.new_objects:
            self.new_objects.remove(domain_object)
            return
        self.dirty_objects.remove(domain_object)
        self.removed_objects.append(domain_object)

    def insert_new_objects(self):
        for obj in self.new_objects:
            MapperRegistry.getMapper(type(obj)).insert(obj)

    def update_dirty_objects(self):
        for obj in self.dirty_objects:
            MapperRegistry.getMapper(type(obj)).update(obj)

    def delete_removed(self):
        for obj in self.removed_objects:
            MapperRegistry.getMapper(type(obj)).delete(obj)

    def commit(self):
        self.insert_new_objects()
        self.update_dirty_objects()
        self.delete_removed()

    @classmethod
    def create_current_unit(cls):
        cls.current_unit = UnitOfWork()

    @classmethod
    def get_current(cls):
        if not cls.current_unit:
            cls.create_current_unit()

        return cls.current_unit


class Worker(DomainObject):
    @staticmethod
    def create(id, name, tab_num, grade):
        obj = Worker(id, name, tab_num, grade)
        obj.mark_new()
        return obj

    def change_grade(self, new_value):
        self.grade = new_value
        self.mark_dirty()


UnitOfWork.get_current()
mapper = MapperRegistry.getMapper(Worker)
worker = mapper.find_by_tab_num('A003002')
worker.change_grade(10)
UnitOfWork.get_current().commit()



